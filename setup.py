import os
from setuptools import setup, find_packages

#from finddata import find_package_data

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "django-supersized-slides",
    version = read('VERSION.txt'),
    url = 'http://github.com/powellc/django-supersized-slides',
    license = 'BSD',
    description = "Integrate a full-screen supersized slideshow in a Django project.",
    long_description = read('README.md'),

    author = 'Colin Powell',
    author_email = "colin.powell@gmail.com",

    packages = find_packages(),
    #package_data = find_package_data(),

    classifiers = [
        'Development Status :: 3 - Alpha',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP',
    ]
)
