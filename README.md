Django Supersized Slides
=========================

A painfully simple Django app that gives you a slideshow model
and brings in the latest release of Supersized! jQuery slideshow
and the template(s) to put it all together.


